# external dependencies

message("++ Setting up library dependencies")

include_directories(lib/inc)

set(YGG_DEPENDENCIES_SRC ${CMAKE_CURRENT_SOURCE_DIR}/lib/src/glad.c)

if (MSVC)	
    message("++ Setting up MSVC dependencies")
    set(YGG_DEPENDENCIES ${OPENGL_LIBRARIES} ${CMAKE_CURRENT_SOURCE_DIR}/lib/bin/win32/glfw3.lib)
    # add box 2d
    set(YGG_DEPENDENCIES ${YGG_DEPENDENCIES} ${CMAKE_CURRENT_SOURCE_DIR}/lib/bin/win32/box2d.lib)
elseif(MINGW)
	message("++ Setting up MINGW dependencies")
    set(YGG_DEPENDENCIES ${OPENGL_LIBRARIES} ${CMAKE_CURRENT_SOURCE_DIR}/lib/bin/mingw/libglfw3.a)
endif()

# add stb_image library to dependencies
add_library(STB_IMAGE ${CMAKE_CURRENT_SOURCE_DIR}/lib/src/stb_image.cpp)
set(YGG_DEPENDENCIES ${YGG_DEPENDENCIES} STB_IMAGE)


