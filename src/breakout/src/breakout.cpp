#include <ygg/inc/logger.h>
#include <ygg/inc/yggdrasil.hpp>
#include <ygg/inc/input.h>
#include <ygg/inc/window.h>

#include <ve/inc/ve_engine.h>
#include <ve/inc/entity_manager.h>
#include <ve/inc/system_manager.h>
#include <ve/inc/component_manager.h>

#include <chrono>
#include <thread>
#include "gameobject/player.h"
#include "gameobject/ball.h"
#include "gameobject/level.h"

#include "systems/bounds_system.h"
#include "systems/drawable_system.h"
#include "systems/world_system.h"
#include "systems/bricks_system.h"
#include "systems/dynamic_object_system.h"
#include "systems/physics_system.h"
#include "systems/controller_system.h"

#include "component/bounds_component.h"
#include "component/velocity_component.h"
#include "component/drawable_component.h"
#include "component/rigidbody_component.h"
#include "component/world_component.h"
#include "component/collidable_component.h"

#include "controller/hid_controller.h"

struct InitContext
{

};

void SetupECS(VE::Engine& ve, const YGG::Context& context)
{
    ve.GetSystemManager()->AddSystem<CollisionSystem>(0);
    ve.GetSystemManager()->AddSystem<OutOfBoundsSystem>(1);
    ve.GetSystemManager()->AddSystem<DynamicObjectSystem>(5);
    ve.GetSystemManager()->AddSystem<ControllerSystem>(6);

    ve.GetSystemManager()->AddSystem<DrawableSystem>(0, context.RendererPtr);

    auto bricksSystem = ve.GetSystemManager()->AddSystem<BricksSystem>(1);
    bricksSystem->Initialize(ve.GetEntityManager(), &context.RendererPtr->getTextureManager());
}

int main()
{
    VE::Engine ve;
    YGG::Context context;
    LOG_INFO("Started Breakout Application");
    Window window(1920, 1080);
    window.Initialize(&context);
    LOG_INFO("Initialized Window");

    YGG::ManagersContext managersContext(context.RendererPtr);
    LOG_INFO("Got Managers");

    LOG_INFO("Setting up ECS");
    SetupECS(ve, context);

    ControllerSystem* controllerSys = ve.GetSystemManager()->GetSystem<ControllerSystem>();

    Input::Create(window.get());
    LOG_INFO("Initialized InputManager");

    auto player = ve.GetEntityManager()->AddEntity<Player>();    
    player->AddComponent<Bounds>(Bounds::Res::Clamp);
    player->AddComponent<Drawable>("resources/textures/paddle.png", managersContext.TextureManagerPtr);
    player->AddComponent<RigidBody>();

    player->Setup(); 
    auto rb = player->GetComponent<RigidBody>();
    player->AddComponent<Collideable>(new Box(rb->Size));
    
    HIDController hid;
    hid.Initialize();
    hid.TakeControl(player);
    controllerSys->RegisterController(&hid);

    auto ball = ve.GetEntityManager()->AddEntity<Ball>();
    ball->AddComponent<Bounds>(Bounds::Res::Bounce);
    ball->AddComponent<Drawable>("resources/textures/ball.png", managersContext.TextureManagerPtr);
    ball->AddComponent<RigidBody>();
    ball->Setup();
    rb = ball->GetComponent<RigidBody>();
    ball->AddComponent<Collideable>(new Sphere(rb->Size.x));

    std::chrono::duration<float, std::milli> elapsedTime(0);

    bool running = true;

    do {
        auto startTime = std::chrono::high_resolution_clock::now();

        ve.GetSystemManager()->Update(elapsedTime.count());

        HERMOD::EventManager::Instance()->PopEvents();

        running = window.Tick();        
        elapsedTime = std::chrono::high_resolution_clock::now() - startTime;


        /*if (elapsedTime.count() < 16.f)
        {
            auto duration = std::chrono::duration<float, std::deci>(16.f - elapsedTime.count());
            std::this_thread::sleep_for<float, std::milli>(duration);
        }*/

    } while(running);

    return 0;
}