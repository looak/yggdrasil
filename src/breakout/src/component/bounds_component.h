#pragma once

#include <ve/inc/component.h>

class Bounds : public VE::Component<Bounds>
{
public:
    enum Res
    {
        Clamp,
        Bounce,
    } Resolve;
    Bounds(Res val) : Resolve(val) {};     
};
