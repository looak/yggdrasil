#include "collidable_component.h"

bool IntersectionBoxVsSphere(const Box* box, const Sphere* sphere)
{
    bool first = abs(box->Position.x - sphere->Position.x) * 2 < (box->Extents.x + sphere->Radius);
    float absPos = abs(box->Position.y - sphere->Position.y);
    bool scnd =  absPos * 2 < (box->Extents.y + sphere->Radius);
	return first && scnd;
};

bool Sphere::Intersects(const IShape* other) const
{
    if(other->Shape == ShapeType::Sphere_Type)
        return IntersectsWithSphere((const Sphere*)other);
    if(other->Shape == ShapeType::Box_Type)
        return IntersectionBoxVsSphere((const Box*)other, this);

    return false;
}

bool Sphere::IntersectsWithSphere(const Sphere* other) const
{
    return 	(abs(Position.x - other->Position.x) * 2 < (Radius + other->Radius)) &&
            (abs(Position.y - other->Position.y) * 2 < (Radius + other->Radius));
}

bool Box::Intersects(const IShape* other) const
{
    if(other->Shape == ShapeType::Box_Type)
        return IntersectsWithBox((const Box*)other);
    if(other->Shape == ShapeType::Sphere_Type)
        return IntersectsWithSphere((const Sphere*)other);
    return false;
}

bool Box::IntersectsWithBox(const Box* other) const
{
    return 	(abs(Position.x - other->Position.x) * 2 < (Extents.x + other->Extents.x)) &&
            (abs(Position.y - other->Position.y) * 2 < (Extents.y + other->Extents.y));
}

bool Box::IntersectsWithSphere(const Sphere* other) const
{
    return IntersectionBoxVsSphere(this, other);
}