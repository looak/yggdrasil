#pragma once

#include <glm/glm.hpp>
#include <ve/inc/component.h>

class Box;
class Sphere;

class IShape
{
public:
	enum ShapeType
	{
		Box_Type,
		Sphere_Type,
	} Shape;

	virtual bool Intersects(const IShape* other) const = 0;
	virtual void SetPosition(glm::vec2 pos) = 0;
};

class Sphere : public IShape
{
public:
	Sphere(float radius) : Radius(radius) { Shape = ShapeType::Sphere_Type; }

	virtual bool Intersects(const IShape* other) const override;
	virtual void SetPosition(glm::vec2 pos) override { Position = pos; }

	glm::vec2 Position;	
	float Radius;

private:
	bool IntersectsWithSphere(const Sphere* other) const;
};

class Box : public IShape
{
public:
	Box(glm::vec2 extents) : Extents(extents) { Shape = ShapeType::Box_Type; } 
	
	virtual bool Intersects(const IShape* other) const override;
	virtual void SetPosition(glm::vec2 pos) override { Position = pos; }

	glm::vec2 Position;
	glm::vec2 Extents;

private:
	bool IntersectsWithBox(const Box* other) const;
	bool IntersectsWithSphere(const Sphere* other) const;
};


class  Collideable : public VE::Component<Collideable>
{
public:     
    Collideable(IShape* shape) : Shape(shape) {}

	IShape* Shape;
};