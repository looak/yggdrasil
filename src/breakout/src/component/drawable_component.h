#pragma once

#include <ve/inc/component.h>

#include <ygg/inc/texture_manager.h>
#include <ygg/inc/utils/file_system.h>

#include <string>

class Drawable : public VE::Component<Drawable>
{
public:
    Drawable(std::string texturePath, const TextureManager* texManagerPtr) 
    {        
        m_texturePath = FileSystem::getPath(texturePath);
        TextureID = texManagerPtr->getTexture(m_texturePath);
    };

    std::size_t TextureID;

private:
    std::string m_texturePath;
};