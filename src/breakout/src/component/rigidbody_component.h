#pragma once

#include <glm/glm.hpp>
#include <ve/inc/component.h>

class b2Body;

class RigidBody : public VE::Component<RigidBody>
{
public:
    RigidBody() 
    {
        Orientation = glm::mat2(1.f);
    };
    RigidBody(const glm::vec2 pos, const glm::vec2 size) : Position(pos), Size(size) 
    {
        Orientation = glm::mat2(1.f);
    }
    glm::vec2 Position;
    glm::vec2 Size;
    glm::mat2 Orientation;
    b2Body* PhysicsRep;
};
