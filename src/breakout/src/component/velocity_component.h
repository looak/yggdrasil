#pragma once

#include <glm/glm.hpp>
#include <ve/inc/component.h>

class Velocity : public VE::Component<Velocity>
{
public:
    Velocity() {};
    glm::vec2 Value;
};