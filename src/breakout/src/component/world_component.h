#pragma once

#include <glm/glm.hpp>
#include <ve/inc/component.h>

class b2World;

class World : public VE::Component<World>
{
public:
    glm::vec2 Gravity;
    b2World* PhysicsRep;
};
