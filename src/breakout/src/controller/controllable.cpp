#include "controllable.h"

#include "component/rigidbody_component.h"

#include <ve/inc/component_manager.h>

Controllable::Controllable() :    
    m_rb(nullptr),
    m_timeStep(0.f)
{   
}

void Controllable::MoveForward(Controllable* controllable)
{
    controllable->MoveForward();
}

void Controllable::MoveLeft(Controllable* controllable)
{
    controllable->MoveLeft();
}

void Controllable::MoveRight(Controllable* controllable)
{
    controllable->MoveRight();
}

void Controllable::MoveUp(Controllable* controllable)
{
    controllable->MoveUp();
}

void Controllable::MoveDown(Controllable* controllable)
{
    controllable->MoveDown();
}

void Controllable::MoveForward() 
{

}
void Controllable::MoveLeft() 
{
    m_rb->Position.x += m_timeStep * -1.f;
}
void Controllable::MoveRight() 
{
    m_rb->Position.x += m_timeStep * 1.f;
}

void Controllable::MoveUp()
{
    m_rb->Position.y += m_timeStep * 1.f;
}
void Controllable::MoveDown()
{
    m_rb->Position.y += m_timeStep * -1.f;
}