#pragma once

#include "gameobject/game_object.h"

class RigidBody;
namespace VE{class ComponentManager;}

class Controllable : public GameObject
{
public:
    Controllable();

    void SetTimeStep(const float timeStep) { m_timeStep = timeStep; };

    static void MoveForward(Controllable* controllable);
    static void MoveLeft(Controllable* controllable);
    static void MoveRight(Controllable* controllable);
    static void MoveUp(Controllable* controllable);
    static void MoveDown(Controllable* controllable);

protected:
    RigidBody* m_rb;

private:
    void MoveForward();
    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();

    float m_timeStep; // delta time;

};