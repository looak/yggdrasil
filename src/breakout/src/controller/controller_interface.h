#pragma once

class Controllable;

class IController
{
public:
    IController() = default;
    virtual ~IController() = default;

    virtual void Update(float dt) = 0;
    virtual void TakeControl(Controllable* controllable) = 0;
    virtual void ReleaseControl() = 0;
};