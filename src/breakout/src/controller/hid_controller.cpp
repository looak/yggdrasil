#include "hid_controller.h"
#include "controllable.h"
#include <glfw/glfw3.h>
#include <ygg/inc/input.h>

HIDController::~HIDController()
{    
    m_actions.clear();
    m_controllablePtr = nullptr;    
}

void HIDController::Initialize()
{
    m_actions[GLFW_KEY_LEFT]    =   &Controllable::MoveLeft;
    m_actions[GLFW_KEY_RIGHT]   =   &Controllable::MoveRight;
    m_actions[GLFW_KEY_UP]      =   &Controllable::MoveUp;
    m_actions[GLFW_KEY_DOWN]    =   &Controllable::MoveDown;    
}

void HIDController::Update(float dt)
{
	Input* input = Input::Instance();
    m_controllablePtr->SetTimeStep(dt);
    for(auto action : m_actions)
    {
        if(input->GetKeyDown(action.first))
            action.second(m_controllablePtr);
    }

    m_controllablePtr->SetTimeStep(0);
}

void HIDController::TakeControl(Controllable* controllable)
{
    m_controllablePtr = controllable;
}

void HIDController::ReleaseControl()
{
    m_actions.clear();
    m_controllablePtr = nullptr;
}