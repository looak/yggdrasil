#pragma once
#include <map>
#include <ygg/inc/platform.h>

#include "controller_interface.h"

class HIDController : public IController
{
public:
    virtual ~HIDController();
    void Initialize();
    virtual void Update(float dt) override;
    virtual void TakeControl(Controllable* controllable) override;
    virtual void ReleaseControl() override;

private:
    Controllable* m_controllablePtr;

    typedef void(*Action)(Controllable*);
    std::map<u32, Action> m_actions;
};