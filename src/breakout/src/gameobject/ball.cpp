#include "ball.h"

#include <ve/inc/component_manager.h>

#include <ygg/inc/texture_manager.h>
#include <ygg/inc/utils/file_system.h>
#include <ygg/inc/debugRenderer.h>

#include "level.h"
#include "component/velocity_component.h"
#include "component/rigidbody_component.h"

#include <glm/gtc/matrix_transform.hpp>

Ball::Ball()
{
    RegisterEventCallback(&Ball::OnCollision);    
}

void
Ball::Setup()
{    
    auto vel = this->AddComponent<Velocity>();
    auto rb = this->GetComponent<RigidBody>();
    m_radius = 12.f;
    rb->Size = glm::vec2(m_radius * 2.f, m_radius * 2.f);
    rb->Position = glm::vec2(0.f, -400.f);
    vel->Value = glm::vec2(0.f, 1.f);
}
/*
YGG::DrawJob
Ball::Update(float elapsedTime, const Level& levelRef)
{
	DebugRenderer& debugRenderer = DebugRenderer::GetInstance();
	debugRenderer.DrawLine(m_position, m_position + m_velocity * glm::vec2(30.0));
	debugRenderer.DrawCicle(glm::vec2(0.0, 0.0), 500.0f);

    // do update    
    YGG::DrawJob drawJob;
    drawJob.Position = m_position;
    drawJob.TextureId = m_textureId;
    drawJob.Size = m_size;
    return drawJob;
}*/

u32 Ball::VectorDirection(const glm::vec2& target) const
{
    static glm::vec2 compass[] = {
        glm::vec2(0.0f, 1.0f),	// up
        glm::vec2(1.0f, 0.0f),	// right
        glm::vec2(0.0f, -1.0f),	// down
        glm::vec2(-1.0f, 0.0f)	// left
    };

    float max = 0.0f;
    int best = -1;
    for (u32 i = 0; i < 4; i++)
    {
        float dp = glm::dot(glm::normalize(target), compass[i]);
        if (dp > max)
        {
            max = dp;
            best = i;
        }
    }
    return best;
} 

void Ball::OnCollision(const CollisionEvent* const event)
{
    if(event->FirstEntitiy == MyID || event->SecondEntity == MyID)
    {
        u64 otherID = event->FirstEntitiy == MyID ? event->SecondEntity : event->FirstEntitiy;
        const auto otherRB = event->ptr_ComponentManager->GetComponent<RigidBody>(otherID);
        auto ballRB = event->ptr_ComponentManager->GetComponent<RigidBody>(MyID);
        auto vel = event->ptr_ComponentManager->GetComponent<Velocity>(MyID);

        int dir = VectorDirection(vel->Value);

        if (dir == 1 || dir == 3) // Horizontal collision
        {
            vel->Value.x = -vel->Value.x; // Reverse horizontal velocity            
        }
        else // Vertical collision
        {
            vel->Value.y = -vel->Value.y; // Reverse vertical velocity
        }
/*
        const glm::vec2 right = otherRB->Orientation[0];
        auto dotPrd = glm::dot(glm::normalize(right), glm::normalize(vel->Value));
        auto angle = std::acos(dotPrd);
        glm::mat2 rotation;
        rotation[0][0] = std::cos(angle);
        rotation[0][1] = -std::sin(angle);
        rotation[1][0] = std::sin(angle);
        rotation[1][1] = std::cos(angle);

        ballRB->Orientation *= rotation;
        vel->Value = ballRB->Orientation[0]; */
    }
}
