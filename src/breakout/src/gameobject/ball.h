#pragma once

#include <ygg/inc/yggdrasil.hpp>
#include <glm/vec2.hpp>
#include <hermod/inc/event_subscriber.h>

#include "systems/physics_system.h"
#include "game_object.h"

class Level;
class Bounds;

class Ball : public GameObject, public HERMOD::IEventSubscriber
{
public:
    Ball();
    ~Ball() = default;

    void Setup();
    //YGG::DrawJob Update(float elapsedTime, const Level& levelRef);


    u32 VectorDirection(const glm::vec2& target) const;
    void OnCollision(const CollisionEvent* const event);

	u64 MyID;
	  

private:
    float m_radius;
};
