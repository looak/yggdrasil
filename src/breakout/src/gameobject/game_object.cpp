#include "game_object.h"

#include <ve/inc/component_manager.h>

static ObjectID s_gameObjectIDTracker = 0;


GameObject::GameObject() :
    GameObjectID(s_gameObjectIDTracker++)
{

}