#pragma once

#include <ygg\inc\platform.h>
#include <ve\inc\entity.h>

namespace VE
{
    class ComponentManager;
}

class GameObject : public VE::Entity
{
public:
    GameObject();
    virtual ~GameObject() = default;
    
protected:
    const ObjectID GameObjectID;
};
