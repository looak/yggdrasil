#include "level.h"
#include "ball.h"
#include <glm/geometric.hpp>
#include <ygg/inc/utils/file_system.h>
#include <ygg/inc/texture_manager.h>


Level::Level()
{

}

Level::~Level()
{
    
}

void 
Level::Setup(const YGG::ManagersContext& context)
{
    m_minBoundry = glm::vec2(-960.f, -540.f);
    m_maxBoundry = glm::vec2(960.f, 540.f);
}

std::vector<YGG::DrawJob>
Level::Update(float elapsedTime, Ball& ball)
{
    std::vector<YGG::DrawJob> result;
       
    return result;
}

glm::vec4
Level::getBoundries() const
{
    return glm::vec4(m_minBoundry, m_maxBoundry);
}