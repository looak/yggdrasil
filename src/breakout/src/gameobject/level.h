#pragma once
#include <ygg/inc/yggdrasil.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <vector>

class Ball;

class Level
{
public:
    Level();
    ~Level();

    void Setup(const YGG::ManagersContext& context);
    std::vector<YGG::DrawJob> Update(float elapsedTime, Ball& ball); 

    glm::vec4 getBoundries() const;

private:

    glm::vec2 m_maxBoundry;
    glm::vec2 m_minBoundry;
};

