#include "player.h"

#include "component/rigidbody_component.h"
#include <ve/inc/component_manager.h>

Player::Player()
{
}

Player::~Player()
{

}

void
Player::Setup()
{
    m_rb = this->GetComponent<RigidBody>();
    m_rb->Size = glm::vec2(128.f, 24.f);    
    m_rb->Position = glm::vec2(0.f,-520.f);
}
