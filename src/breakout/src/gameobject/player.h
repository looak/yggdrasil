#pragma once

#include <ygg/inc/yggdrasil.hpp>
#include <ygg/inc/input.h>
#include <glm/vec2.hpp>
#include "glfw/glfw3.h"
#include "controller/controllable.h"

class Level;
class Ball;

class Player : public Controllable
{
public:
	Player();
	~Player();

	void Setup();


};
