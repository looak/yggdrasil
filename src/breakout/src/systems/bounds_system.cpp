#include "bounds_system.h"
#include <ve/inc/component_manager.h>

#include "component/bounds_component.h"
#include "component/rigidbody_component.h"
#include "component/velocity_component.h"


void OutOfBoundsSystem::Resolve()
{

}

void OutOfBoundsSystem::Update(f32 dt)
{
    auto boundsComponents = GetComponentManager()->GetComponents<Bounds>();

    auto minBoundry = glm::vec2(-960.f, -540.f);
    auto maxBoundry = glm::vec2(960.f, 540.f);
    auto boundries = glm::vec4(minBoundry, maxBoundry);

    for(auto component : boundsComponents)
    {   
        Bounds* bounds = (Bounds*)component;
        auto rb = (RigidBody*)GetComponentManager()->GetComponent<RigidBody>(bounds->GetOwner());

        if(bounds->Resolve == Bounds::Res::Bounce)
        {
            auto vel = (Velocity*)GetComponentManager()->GetComponent<Velocity>(bounds->GetOwner());
            if(rb->Position.x + (rb->Size.x * .5f) > boundries.z || rb->Position.x - (rb->Size.x * .5f) < boundries.x)
                vel->Value.x *= -1.f;
            else
            if(rb->Position.y + (rb->Size.x * .5f) > boundries.w || rb->Position.y - (rb->Size.x * .5f) < boundries.y)
                vel->Value.y *= -1.f;
        }
        else
        if(bounds->Resolve == Bounds::Res::Clamp)
        {
            if(rb->Position.x + (rb->Size.x * .5f) > boundries.z)
                rb->Position.x = boundries.z - (rb->Size.x * .5f);
            else if(rb->Position.x - (rb->Size.x * .5f) < boundries.x)
                rb->Position.x = boundries.x + (rb->Size.x * .5f);
        }
    }
}
