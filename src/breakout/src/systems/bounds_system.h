#pragma once

#include <ve/inc/system.h>

class OutOfBoundsSystem : public VE::System<OutOfBoundsSystem>
{
public:
	virtual void Update(f32 dt) override;

private:
	void Resolve();
};