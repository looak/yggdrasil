#include "bricks_system.h"

#include <ve/inc/component_manager.h>
#include <ve/inc/entity_manager.h>
#include <ygg/inc/texture_manager.h>

#include "component/rigidbody_component.h"
#include "component/drawable_component.h"
#include "component/collidable_component.h"


BricksSystem::BricksSystem()
{

}

void BricksSystem::Initialize(VE::EntityManager* entityManager, const TextureManager* texManagerPtr)
{
    RegisterEventCallback(&BricksSystem::OnCollision);

    auto componentManager = GetComponentManager();
    glm::vec2 size = {128.f, 32.f};

    for(int x = 0; x < 15; ++x)
    {
//        for(int y = 0; y < 8; ++y)
        {
            auto brick = entityManager->AddEntity<Brick>();
            
            float posX = (x * 128.f) - 896.f;
            float posY = 128.f; // (y * 32.f) + 128.f;
            glm::vec2 pos = {posX, posY };
            brick->AddComponent<RigidBody>(pos, size);
            brick->AddComponent<Drawable>("resources/textures/block.png", texManagerPtr);
            brick->AddComponent<Collideable>(new Box(size));

            m_brickIDs.emplace(brick->GetEntityID());
        }
    }

}

void BricksSystem::Update(f32 dt)
{

}

void BricksSystem::OnCollision(const CollisionEvent* const event)
{
    u64 ID = ContainsID(event);
    if(ID == INVALID_OBJECT_ID)
        return;

    event->ptr_ComponentManager->SetActiveAssociated(ID, false); 
}

u64 BricksSystem::ContainsID(const CollisionEvent* const event)
{
    if(m_brickIDs.find(event->FirstEntitiy) != m_brickIDs.end())
        return event->FirstEntitiy;
    if(m_brickIDs.find(event->SecondEntity) != m_brickIDs.end())
        return event->SecondEntity;

    return INVALID_OBJECT_ID;
}