#pragma once

#include <ve/inc/system.h>
#include <hermod/inc/event_subscriber.h>

#include "systems/physics_system.h"
#include <unordered_set>

#include "gameobject/game_object.h"

namespace VE{
class EntityManager;
}
class TextureManager;

class Brick : public GameObject
{
};

class BricksSystem : public VE::System<BricksSystem>, public HERMOD::IEventSubscriber
{
public:
    BricksSystem();

    void Initialize(VE::EntityManager* entityManager, const TextureManager* texManagerPtr);
    virtual void Update(f32 dt) final;

    void OnCollision(const CollisionEvent* const event);

private:
    u64 ContainsID(const CollisionEvent* const event);
    std::unordered_set<u64> m_brickIDs;
};