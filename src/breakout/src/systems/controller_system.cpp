#include "controller_system.h"
#include "controller/controller_interface.h"

void ControllerSystem::Update(f32 dt)
{
    for(auto controller : m_controllers)
        controller->Update(dt);
}

void ControllerSystem::RegisterController(IController* controller)
{
    m_controllers.push_back(controller);
}

void ControllerSystem::UnregisterController(IController* controller)
{
    m_controllers.remove(controller);
}
