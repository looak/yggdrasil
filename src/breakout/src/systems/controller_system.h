#pragma once
#include <ve/inc/system.h>

class IController;

class ControllerSystem : public VE::System<ControllerSystem>
{
public:
	virtual void Update(f32 dt) final;

	void RegisterController(IController* controller);
	void UnregisterController(IController* controller);

private:
	std::list<IController*> m_controllers;
};