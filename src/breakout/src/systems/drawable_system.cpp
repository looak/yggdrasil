#include "drawable_system.h"
#include <ve/inc/component_manager.h>
#include <ygg/inc/yggdrasil.hpp>
#include <ygg/inc/renderer.h>

#include "component/drawable_component.h"
#include "component/rigidbody_component.h"

DrawableSystem::DrawableSystem(Renderer* rendererPtr)
    : m_rendererPtr(rendererPtr)
{
}

DrawableSystem::~DrawableSystem()
{
    m_rendererPtr = nullptr;
}
    
void DrawableSystem::Update(f32 dt)
{
    std::vector<YGG::DrawJob> frame;
    auto drawableComponents = GetComponentManager()->GetComponents<Drawable>();

    for(auto component : drawableComponents)
    {
        if(!component->IsActive())
            continue;
        YGG::DrawJob drawJob;    
        auto drawable = (Drawable*)component;
        auto rb = (RigidBody*)GetComponentManager()->GetComponent<RigidBody>(drawable->GetOwner());
        drawJob.TextureId = drawable->TextureID;
        drawJob.Position = rb->Position;
        drawJob.Size = rb->Size;
        frame.push_back(drawJob);
    }

    m_rendererPtr->SetDrawContext(frame);
}