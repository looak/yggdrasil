#pragma once

#include <ve/inc/system.h>

class Renderer;

class DrawableSystem : public VE::System<DrawableSystem>
{
public:
    DrawableSystem(Renderer* rendererPtr);
    ~DrawableSystem();

	virtual void Update(f32 dt) override;

private:
    Renderer* m_rendererPtr;

};