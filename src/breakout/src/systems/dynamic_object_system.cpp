#include "dynamic_object_system.h"

#include <ve/inc/component_manager.h>

#include "component/velocity_component.h"
#include "component/rigidbody_component.h"
#include "component/collidable_component.h"

void DynamicObjectSystem::Update(f32 dt)
{
    auto velocities = GetComponentManager()->GetComponents<Velocity>();

    for(auto component : velocities) 
    {
        auto vel = (Velocity*)component;
        auto rb = GetComponentManager()->GetComponent<RigidBody>(vel->GetOwner());

        rb->Position += vel->Value * dt;
    }
}