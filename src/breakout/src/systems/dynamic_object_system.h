#pragma once

#include <ve/inc/system.h>

class DynamicObjectSystem : public VE::System<DynamicObjectSystem>
{
    virtual void Update(f32 dt) final;
};
