#include "physics_system.h"

#include "component/collidable_component.h"
#include "component/rigidbody_component.h"

#include <hermod/inc/event_manager.h>
#include <ve/inc/component_manager.h>

#include <vector>

void CollisionSystem::Update(f32 dt)
{
    std::vector<CollisionID> resolvedIDs;

    auto collsFirst = GetComponentManager()->GetComponents<Collideable>();

    for (auto first : collsFirst)
    {
        if(!first->IsActive())
            continue;

        auto collideableFirst = (Collideable*)first;
        auto pbFrst = GetComponentManager()->GetComponent<RigidBody>(first->GetOwner());
        
        // we need to sync position between rb and collideable, this shouldn't be done like this.
        collideableFirst->Shape->SetPosition(pbFrst->Position);

        auto collsSecond = GetComponentManager()->GetComponents<Collideable>();
        for (const auto second : collsSecond)
        {
            if(!second->IsActive())
                continue;

            if (collideableFirst->GetOwner() != second->GetOwner())
            {
                auto collideableSecond = (Collideable*)second;
                auto pbScnd = GetComponentManager()->GetComponent<RigidBody>(second->GetOwner());
                // we need to sync position between rb and collideable, this shouldn't be done like this.
                collideableSecond->Shape->SetPosition(pbScnd->Position);

                if (collideableFirst->Shape->Intersects(collideableSecond->Shape))
                {   
                    // since we're going through the list twice we'll end up with duplicate collisions, this logic is to avoid sending the second event.
                    u64 collAdd = collideableFirst->GetOwner() + collideableSecond->GetOwner();
                    auto collID = std::make_tuple(collAdd, collideableFirst->GetOwner(), collideableSecond->GetOwner());
                    if(HasCollided(resolvedIDs, collID))
                        continue;

                    CollisionEvent* event = new CollisionEvent();
                    event->FirstEntitiy = collideableFirst->GetOwner();
                    event->SecondEntity = collideableSecond->GetOwner();
                    event->ptr_ComponentManager = GetComponentManager();
                    HERMOD::EventManager::Instance()->DispatchEvent(event);
                    resolvedIDs.push_back(collID);
                }
            }            
        }
    }
}

bool CollisionSystem::HasCollided(std::vector<CollisionID> collisions, CollisionID id)
{
    for (auto collided : collisions)
    {
        if (std::get<0>(collided) == std::get<0>(id))
        {
            if ((std::get<1>(collided) == std::get<1>(id) || std::get<1>(collided) == std::get<2>(id)) &&
                (std::get<2>(collided) == std::get<1>(id) || std::get<2>(collided) == std::get<2>(id)))
                return true;
        }
    }

    return false;
}
