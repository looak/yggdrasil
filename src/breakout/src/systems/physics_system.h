#pragma once

#include <ve/inc/system.h>
#include <hermod/inc/event.h>

#include <tuple>

class IShape;

class CollisionEvent : public HERMOD::Event<CollisionEvent>
{
public:
    u64 FirstEntitiy;
	u64 SecondEntity;
	VE::ComponentManager* ptr_ComponentManager;
};

class CollisionSystem : public VE::System<CollisionSystem>
{
typedef std::tuple<u64, u64, u64> CollisionID;

public:
	virtual void Update(f32 dt) override;
	
private:

	bool HasCollided(std::vector<CollisionID> collisions, CollisionID id);
};