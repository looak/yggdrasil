#include "world_system.h"
#include "component/world_component.h"

#include <Box2D/Box2D.h>

#include <ve/inc/component_manager.h>

void WorldSystem::Initialize(u64 entityId)
{
    auto world = GetComponentManager()->GetComponent<World>(entityId);
    auto g = world->Gravity;
    world->PhysicsRep = new b2World(b2Vec2(g.x, g.y));

    m_world = world;

    RegisterEventCallback(&WorldSystem::OnEventCallback);
}

void WorldSystem::Update(f32 dt)
{    

}

void WorldSystem::OnEventCallback(const WorldSystemEvent* const event)
{

}
