#pragma once
#include <ve/inc/system.h>

#include <hermod/inc/event_subscriber.h>
#include <hermod/inc/event.h>

class World;

class WorldSystemEvent : public HERMOD::Event<WorldSystemEvent>
{
public:
    int SomeValue;
};

class WorldSystem : public VE::System<WorldSystem>, public HERMOD::IEventSubscriber
{
public:
    void Initialize(u64 entitId);
	virtual void Update(f32 dt) final;

    void OnEventCallback(const WorldSystemEvent* const event);

private:

    World* m_world;

};