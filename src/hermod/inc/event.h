#pragma once

#include <ygg/inc/platform.h>
#include <ve/inc/id_tracker.h>

namespace HERMOD
{

typedef u64 EventID;

class IEvent
{
public:
    virtual EventID GetEventTypeID() const = 0;
};

template<class T>
class Event : public IEvent
{
public:
	static const EventID EventTypeID;

    virtual EventID GetEventTypeID() const final { return EventTypeID; }
	
	Event() = default;
	virtual ~Event() = default;
};

template<class T>
const TypeID HERMOD::Event<T>::EventTypeID = VE::IDTracker<IEvent>::Get<T>();
}
