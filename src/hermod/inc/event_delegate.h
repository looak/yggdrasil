#pragma once

#include <ygg/inc/platform.h>

namespace HERMOD
{

class IEvent;

class IEventDelegate
{
public:
    virtual void Invoke(const IEvent* const e) = 0;
    virtual u64 GetEventID() = 0;
};

template<class Class, class EventType>
class EventDelegate : public IEventDelegate
{   
    typedef void(Class::*Callback)(const EventType* const);

    Class* m_target;
    Callback m_method;
    u64 m_eventID;

public:
    EventDelegate(Class* receiver, Callback& callbackFunction) :
        m_target(receiver),
        m_method(callbackFunction)
    {
        m_eventID = EventType::EventTypeID;
    }

    virtual void Invoke(const IEvent* const e) final
    {
        (m_target->*m_method)(reinterpret_cast<const EventType* const>(e));
    }

    virtual u64 GetEventID() final
    {
        return m_eventID;
    }

};

}