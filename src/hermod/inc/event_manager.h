#pragma once

#include "event.h"
#include "event_delegate.h"

#include <map>
#include <queue>

namespace HERMOD
{

class EventManager
{
public:
    ~EventManager();

    static EventManager* Instance();

    void RegisterEventDelegate(IEventDelegate* subscriber);
    void DispatchEvent(const IEvent* const event);

    void PopEvents();

private:
    EventManager() = default;
    static EventManager* m_instance;

    std::map<EventID, std::vector<IEventDelegate*>> m_eventDelegates;

    std::queue<const IEvent*> m_queuedEvents;
};

}