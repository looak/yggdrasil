#pragma once

#include "event_manager.h"
#include "event_delegate.h"

namespace HERMOD
{

class IEventSubscriber
{
public:

    virtual ~IEventSubscriber() = default;

    template<class E, class C>
    inline void RegisterEventCallback(void(C::*Callback)(const E* const))
    {
        IEventDelegate* eventDelegate = new EventDelegate<C, E>(static_cast<C*>(this), Callback);

        m_callbacks.push_back(eventDelegate);
        EventManager::Instance()->RegisterEventDelegate(eventDelegate);
    }


private:
    std::vector<IEventDelegate*> m_callbacks;
    
};

}   