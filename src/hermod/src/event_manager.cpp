#include "event_manager.h"

using namespace HERMOD;

EventManager* EventManager::m_instance = nullptr;

EventManager::~EventManager()
{

}

EventManager* EventManager::Instance()
{
    if(m_instance == nullptr)
        m_instance = new EventManager();
    return m_instance;
}

void EventManager::RegisterEventDelegate(IEventDelegate* subscriber)
{
    auto keyID = subscriber->GetEventID();
    auto it = m_eventDelegates.find(subscriber->GetEventID());
    if(it == m_eventDelegates.end())
    {
        m_eventDelegates[keyID] = { subscriber };
    }
    else
    {
        m_eventDelegates.at(keyID).push_back(subscriber);
    }    
}

void EventManager::DispatchEvent(const IEvent* event)
{
    m_queuedEvents.push(event);
}

void EventManager::PopEvents()
{
    if(m_queuedEvents.empty())
        return;

    auto event = m_queuedEvents.front();
    auto key = event->GetEventTypeID();
    for(auto delegate : m_eventDelegates.at(key))
    {
        delegate->Invoke(event);
    }
    m_queuedEvents.pop();
}