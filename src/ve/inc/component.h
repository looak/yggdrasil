#pragma once
#include "entity.h"
#include "id_tracker.h"

namespace VE
{

typedef ObjectID ComponentID;

class IComponent
{
friend class ComponentContainer;
friend class ComponentManager;

protected:	
	ComponentID		m_componentID;
	EntityID		m_owner;
	bool			m_enabled;

public:
	IComponent() = default;
	virtual ~IComponent() = default;

	// ACCESSOR
	inline const ComponentID GetComponentID() const { return this->m_componentID; }
	inline const EntityID GetOwner() const { return this->m_owner; }
	inline void SetActive(bool state) { this->m_enabled = state; }
	inline bool IsActive() const { return this->m_enabled; }
};

template<class T>
class Component : public IComponent
{
public:
	static const TypeID ComponentTypeID;
	
	Component() = default;
	virtual ~Component() = default;
};

// This private member only exists to force the compiler to create an instance of Component T,
// which will set its unique identifier.
template<class T>
const TypeID Component<T>::ComponentTypeID = IDTracker<IComponent>::Get<T>();

} // namespace VE