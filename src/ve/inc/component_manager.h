#pragma once

#include "component.h"
#include <ygg/inc/logger.h>
#include <sstream>

namespace VE
{

class ComponentManager
{
private:
    class IComponentContainer
    {
    public:

        virtual ~IComponentContainer() {};
        
        virtual const char* GetComponentContainerTypeName() const = 0;
        virtual void DestroyComponent(IComponent* object) = 0;
        virtual void RemoveComponentsAssociated(const EntityID entitID) = 0;
        virtual void SetActiveAssociated(const EntityID entitID, bool active) = 0;
    };

    template<class T>
    class ComponentContainer : public IComponentContainer
    {
        ComponentContainer(const ComponentContainer&) = delete;
        ComponentContainer& operator=(ComponentContainer&) = delete;

    public:
        ComponentContainer() {
            m_components.reserve(512);
        };
        ~ComponentContainer() 
        { 
            for(auto comp : m_components)
                delete comp;
        }


        const char* GetComponentContainerTypeName() const final
        {
            static const char* COMPONENT_TYPE_NAME{ typeid(T).name() };
            return COMPONENT_TYPE_NAME;
        }		

        template<class... ARGS>
        ComponentID CreateComponent(const EntityID entityId, ARGS&&... args)
        {
            T* component = new T(std::forward<ARGS>(args)...);
            component->m_owner = entityId;
            component->m_componentID = m_components.size();
            component->m_enabled = true;
            m_components.push_back(component);
            return component->GetComponentID();
        }

        void DestroyComponent(IComponent* object)
        {	
            delete object;
            object = nullptr;
        }
        void RemoveComponentsAssociated(const EntityID entityID)
        {
            auto itr = find_if(m_components.begin(), m_components.end(), 
                [&entityID](const IComponent* comp)
                {
                    return comp->GetOwner() == entityID;
                });

            if(itr != m_components.end())
            {
                auto tmpPtr = *itr;
                m_components.erase(itr);
                DestroyComponent(tmpPtr);
            }
        }
        
        void SetActiveAssociated(const EntityID entityID, bool active)
        {
            auto itr = find_if(m_components.begin(), m_components.end(), 
                [&entityID](const IComponent* comp)
                {
                    return comp->GetOwner() == entityID;
                });

            if(itr != m_components.end())
                (*itr)->SetActive(active);
        }

        std::vector<IComponent*>& Get()
        {
            return m_components;
        }

        std::vector<IComponent*> m_components;
    }; // class ComponentContainer

public:
    template<class T, class... ARGS>
    T* AddComponent(const EntityID entityID, ARGS&&... args)
    {
        InternalAddComponent<T>(entityID, std::forward<ARGS>(args)...);
        return GetComponent<T>(entityID);
    }

    void RemoveComponentsAssociated(const EntityID entityID);
    void SetActiveAssociated(const EntityID entityID, bool active);

    template<class T>
    std::vector<IComponent*>& GetComponents()
    {
        return GetComponentContainer<T>()->Get();
    }

    template<class T>
    T* GetComponent(const EntityID& entityID)
    {
        auto components = GetComponentContainer<T>();
        for(auto component : components->Get())
        {
            if(component == nullptr)
                continue;
            if(component->GetOwner() == entityID)
                return (T*)component;
        }
        LOG_WARNING("Couldn't find expected component");
        return nullptr;
    }

    template<class T>
    const T* GetConstComponent(const EntityID& entityID)
    {
        return GetComponent<T>(entityID);
    }

private:
    template<class T, class... ARGS>
    ComponentID InternalAddComponent(const EntityID entityID, ARGS&&... args)
    {
        std::ostringstream ssInfo;
        ssInfo << "Creating ";
        ssInfo << typeid(T).name();
        ssInfo << " and registering it to " << entityID;
        LOG_INFO(ssInfo.str().c_str());

        auto container = GetComponentContainer<T>();
        return container->CreateComponent(entityID, std::forward<ARGS>(args)...);
    }

    template<class T>
    ComponentContainer<T>* GetComponentContainer()
    {
        TypeID CID = T::ComponentTypeID;

        auto it = this->m_containers.find(CID);
        ComponentContainer<T>* cc = nullptr;

        if (it == this->m_containers.end())
        {
            cc = new ComponentContainer<T>();
            this->m_containers[CID] = cc;
        }
        else
            cc = static_cast<ComponentContainer<T>*>(it->second);

        FATAL_ASSERT_EXPR(cc != nullptr, "Failed to create ComponentContainer<T>!");
        return cc;
    }

    std::map<TypeID, IComponentContainer*> m_containers;
    
};

} // namespace VE