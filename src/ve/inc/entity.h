#pragma once
#include <ygg/inc/platform.h>


namespace VE
{

class EntityManager; 
class ComponentManager;

typedef u64 EntityID;

class IEntity
{
friend class EntityManager;
public:
	IEntity() : m_componentManagerPtr(nullptr), m_id(-1) {};
	virtual ~IEntity() = default;
	inline const EntityID GetEntityID() const { return this->m_id; }

protected:
    ComponentManager* GetComponentManager() const {return m_componentManagerPtr; }

private:
	ComponentManager* m_componentManagerPtr;
	EntityID m_id;
};

class Entity : public IEntity
{
public:
	Entity() = default;

	template<class T, class... ARGS>
    T* AddComponent(ARGS&&... args)
	{
		return GetComponentManager()->AddComponent<T>(GetEntityID(), std::forward<ARGS>(args)...);
	}

	template<class T>
	T* GetComponent()
	{
		return GetComponentManager()->GetComponent<T>(GetEntityID());
	}

private:
};

}