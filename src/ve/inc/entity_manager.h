#pragma once
#include <vector>
#include "entity.h"

namespace VE
{

class ComponentManager;

class EntityManager
{
public:
    EntityManager(ComponentManager* componentManager) 
        : m_componentManager(componentManager) 
        {};

    ~EntityManager();
    
    template<class T>
    T* AddEntity()
    {
        EntityID id = m_entities.size();
        auto entity = new T();

        entity->m_id = id;
        entity->m_componentManagerPtr = m_componentManager;

        auto pair = std::make_pair(id, entity);
        m_entities.push_back(pair);
        return entity;    
    }

private:
    std::vector<std::pair<EntityID, IEntity*>> m_entities;

    ComponentManager* m_componentManager;    
};
}