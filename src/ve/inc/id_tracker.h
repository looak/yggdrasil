#pragma once

#include <ygg/inc/platform.h>

namespace VE 
{
    
template<class T>
class IDTracker
{
    static TypeID s_count;

public:

    template<typename U>
    static const TypeID Get()
    {
        static const TypeID STATIC_TYPE_ID { s_count++ };
        return STATIC_TYPE_ID;
    }   
    
    static const TypeID Get()
    {
        return s_count;
    }
};
    template<class T>
    TypeID IDTracker<T>::s_count = 0;

} // namespace VE
