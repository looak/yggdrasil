#pragma once

#include <array>
#include "component.h"
#include "id_tracker.h"

namespace VE
{

class ComponentManager;

typedef TypeID SystemTypeID;

class ISystem
{
friend class SystemManager;

public:
	ISystem() = default;
	virtual ~ISystem() { ptr_componentManager = nullptr; }

	ComponentManager* GetComponentManager() { return ptr_componentManager; };

public: // pure virtual
	virtual void Update(f32 dt)		= 0;
private:

	ComponentManager* ptr_componentManager;
};

template<class T>
class System : public ISystem
{
public:
	System() {};

	static const SystemTypeID SystemID;

// ISystem
	virtual void Update(f32 dt) override {};

private:
	
};

template<class T>
const SystemTypeID System<T>::SystemID = IDTracker<ISystem>::Get<T>();

} // namespace VE