#pragma once
#include "system.h"

#include <ygg/inc/platform.h>
#include <sstream>

/* SystemManager


*/

namespace VE
{

class ComponentManager;

class SystemManager
{
public:
    SystemManager(ComponentManager* compManager) : ptr_componentManager(compManager) {};
    ~SystemManager();

    void Update(f32 deltatime);
    
    /*  Prioritizes higher priortiy values first in update.
        priority : value between 0-255
    */
    template<class T, class... ARGS>
    T* AddSystem(u8 priority, ARGS&&... systemArgs)
    {        
        const TypeID sysId = System<T>::SystemID;

        std::ostringstream ssInfo;
        ssInfo << "Creating ";
        ssInfo << typeid(T).name();
        ssInfo << " with ID: " << sysId;
        ssInfo << ", priority: " << (int)priority;
        LOG_INFO(ssInfo.str().c_str());
        
        auto sysIt = m_systems.find(sysId);
        if (sysIt != m_systems.end())
            FATAL_ASSERT("Can't create more than one system of each type!");
        
        T* retVal = new T(std::forward<ARGS>(systemArgs)...);

        reinterpret_cast<ISystem*>(retVal)->ptr_componentManager = ptr_componentManager;
        m_systems[sysId] = retVal;

        auto sysPrio = std::make_pair(priority, retVal);
        m_systemsPriority.push_back(sysPrio);
        std::sort(m_systemsPriority.begin(), m_systemsPriority.end(), 
            [] (SystemPriority const& a, SystemPriority const& b)
            {
                return a.first > b.first;
            });
        return retVal;
    }

    template<class T>
    T* GetSystem()
    {
        if(m_systems.find(System<T>::SystemID) != m_systems.end())
            return (T*)m_systems.at(System<T>::SystemID);

        return nullptr;
    }

private:
    ComponentManager* ptr_componentManager;

    std::map<TypeID, ISystem*> m_systems;
    typedef std::pair<u8, ISystem*> SystemPriority;
    std::vector<SystemPriority> m_systemsPriority;
};

} // namespace VE