#pragma once
#include <ygg/inc/platform.h>

namespace VE
{
class EntityManager;
class ComponentManager;
class SystemManager;

class Engine
{
public:
    Engine();
    ~Engine();

    void Update(f32 dt);

    EntityManager* GetEntityManager() { return m_entityManager; }
    ComponentManager* GetComponentManager() { return m_componentManager; }
    SystemManager* GetSystemManager() { return m_systemManager; } 

private:

    EntityManager* m_entityManager;
    ComponentManager* m_componentManager;
    SystemManager* m_systemManager;

};
}