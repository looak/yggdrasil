#include "component_manager.h"

using namespace VE;


void ComponentManager::RemoveComponentsAssociated(const EntityID entityID)
{
    for(auto container : m_containers)
    {
        container.second->RemoveComponentsAssociated(entityID);
    }
}

void ComponentManager::SetActiveAssociated(const EntityID entityID, bool active)
{    
    for(auto container : m_containers)
    {
        container.second->SetActiveAssociated(entityID, active);
    }
}