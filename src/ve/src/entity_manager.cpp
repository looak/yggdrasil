#include "entity_manager.h"

using namespace VE;

EntityManager::~EntityManager()
{
    for(auto pair : m_entities)
    {
        delete pair.second;
    }

    m_entities.clear();
}
