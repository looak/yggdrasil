#include "system_manager.h"

using namespace VE;

SystemManager::~SystemManager()
{
    for(auto pair : m_systems)
    {
        delete pair.second;
    }

    m_systems.clear();
    ptr_componentManager = nullptr;
}

void SystemManager::Update(f32 deltatime)
{
    for(auto sysPair : m_systemsPriority)
    {
        auto sysPtr = sysPair.second;
        sysPtr->Update(deltatime);
    }
}