#include "ve_engine.h"

#include "system_manager.h"
#include "component_manager.h"
#include "entity_manager.h"

using namespace VE;

Engine::Engine()
{
    m_componentManager = new ComponentManager();
    m_entityManager = new EntityManager(m_componentManager);
    m_systemManager = new SystemManager(m_componentManager);
}

Engine::~Engine()
{
    delete m_systemManager;
    delete m_componentManager;
    delete m_entityManager;
}

void Engine::Update(f32 dt)
{
    m_systemManager->Update(dt);
}

