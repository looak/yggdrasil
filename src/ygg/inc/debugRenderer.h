#pragma once
#include <vector>
#include <glm/vec2.hpp>
#include <glm/matrix.hpp>

#include "logger.h"

struct GLFWwindow;
class Quad;
class Renderer;

struct ViewData
{
	glm::mat4 view;
	glm::mat4 proj;
};

class DebugRenderer
{
public:
	static DebugRenderer& GetInstance()
	{
		static DebugRenderer debugRenderer;
		return debugRenderer;
	}

	DebugRenderer() = default;
    ~DebugRenderer();

	void DrawPoint(const glm::vec2& position);
	void DrawLine(const glm::vec2& start, const glm::vec2& end);
	void DrawCicle(const glm::vec2& origin, float radius);

    void Draw(const ViewData& viewData);

	void Initialize(Renderer* renderer);

private:
	struct DebugPrimitive
	{
		enum Type
		{
			Point,
			Line,
			Circle,
			Count,
		} type;

		union
		{
			struct { glm::vec2 pos; } point;
			struct { glm::vec2 start, end; } line;
			struct { glm::vec2 origin; float radius; } circle;
		};
	};

	Renderer* m_cachedRenderer = nullptr;

    std::vector<DebugPrimitive> m_debugPrimitives;

	size_t m_shader;
	unsigned int m_vao, m_vertexBuffer, m_indexBuffer;
};

