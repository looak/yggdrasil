#pragma once
#include <functional>
#include <map>
#include <vector>

struct GLFWwindow;

class InputCallbackInterface
{
public:
    virtual ~InputCallbackInterface() {};
    virtual void InputCallback(int key, int action) = 0;
};

class Input
{
public:
	Input(GLFWwindow* window);
    ~Input();
    static void Create(GLFWwindow* window);
    static Input* Instance();

	bool GetKeyDown(int keyCode); // TODO: Type safety would be nice, but requires a wrapper

private:
    static Input* m_instance;
    Input();
    static void KeyCallback(GLFWwindow* window, int key, int scanCode, int action, int modifier);
    void KeyCallback(int key, int scanCode, int action, int modifier);

private:
	GLFWwindow* m_window;

};
