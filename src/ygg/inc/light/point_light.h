#pragma once

#include "resource/shader_program.h"
#include <glm/mat4x4.hpp>
#include "resource/input_layout.h"

class PointLight
{
public:
    PointLight();
    ~PointLight();

    bool Initialize(ShaderProgram* shader);
    void Draw() const;
    std::size_t ShaderToUse() const;

private:    
    std::size_t m_shader;
    ShaderProgram* m_shaderPtr;

    unsigned int m_vertexArrayObject;
    unsigned int m_vertedBuffer;
    unsigned int m_elementBuffer;
};
