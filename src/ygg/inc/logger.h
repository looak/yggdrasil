#pragma once

#include <cstring>
#include <iostream>
#include <cassert>
#include <queue>

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG_ERROR(x) Log::ErrorImpl(x, __FILENAME__, __FUNCTION__, __LINE__)
#define LOG_INFO(x) Log::InfoImpl(x, __FILENAME__, __LINE__)
#define LOG_WARNING(x) Log::WarningImpl(x, __FILENAME__, __FUNCTION__, __LINE__)
#define FATAL_ASSERT_EXPR(expr, x) Log::AssertImpl(expr, x, __FILENAME__, __FUNCTION__, __LINE__)
#define FATAL_ASSERT(x) Log::AssertImpl(0, x, __FILENAME__, __FUNCTION__, __LINE__)


namespace Log
{
    static void InfoImpl(const char* message, const char* file, const int line)
    {
        std::cout << "[      INFO] " << file << ":" << line << " > message: " << message << std::endl;
    }

    static void WarningImpl(const char* message, const char* file, const char* function, const int line)
    {
        std::cout << "[   WARNING] " << file << ":" << line  << " > " << function << " message: " << message << std::endl;
    }

    static void ErrorImpl(const char* message, const char* file, const char* function, const int line)
    {
        std::cout << "[     ERROR] " << file << ":" << line << " > " << function << " message: " << message << std::endl;
    }

    static void AssertImpl(int expression, const char* message, const char* file, const char* function, const int line)
    {
        if (expression == 0)
        {
            std::cout << "[FATAL ASRT] " << file << ":" << line << " > " << function << " message: " << message << std::endl;
            assert(expression);
        }
    }
} // namespace Log

class CurrentLogger
{
public:
    CurrentLogger();
    ~CurrentLogger();

    void Warning(const char* message);

    void Flush();

private:
    std::queue<std::string> m_buffer;

};
