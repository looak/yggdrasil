#pragma once
#include "texture_manager.h"
#include "shader_manager.h"
#include <vector>

struct GLFWwindow;
class Quad;
class PointLight;
class FrameBuffer;

namespace YGG
{
    struct DrawJob;
}

class Renderer
{
public:
    Renderer() = default;
    ~Renderer();

    void Initialize(GLFWwindow* window);
    void SetDrawContext(std::vector<YGG::DrawJob> drawJobs);

    void Draw(GLFWwindow* window);

    const ShaderManager& getShaderManager() const;
    const TextureManager& getTextureManager() const;

private:
    ShaderManager m_shaderManager;
    TextureManager m_textureManager;

    std::vector<YGG::DrawJob> m_drawJobs;

    Quad* m_screenQuad;
    Quad* m_instance;
    PointLight* m_pointLight;

    FrameBuffer* m_firstFrameBuffer;
    FrameBuffer* m_secondFrameBuffer;
};

