#pragma once
#include <map>
#include <string>

class ShaderProgram;

class ShaderManager
{
public:
    ShaderManager() = default;
    ~ShaderManager();

    std::size_t getShader(const std::string& vsLocation, const std::string& fsLocation) const;
    unsigned int getShader(std::size_t hash) const;

// TODO:  remove this getter for ptr.
    const ShaderProgram* getShaderPtrConst(std::size_t hash) const;
    ShaderProgram* getShaderPtr(std::size_t hash) const;

    bool setUniformData(std::size_t hash, const std::string& name, const void* data) const;
    void useShader(std::size_t hash) const;

private:
    typedef std::map<std::size_t, ShaderProgram*> ShaderMap;
    mutable ShaderMap m_shaders;
};

