#pragma once
#include <map>
#include <string>

class Texture;

class TextureManager
{
public:
    TextureManager() = default;
    ~TextureManager();

    std::size_t getTexture(const std::string& texLocation) const;
    unsigned int getTexture(std::size_t hash) const;

    void useTexture(std::size_t hash) const;
private:
    typedef std::map<std::size_t, Texture*> TextureMap;
    mutable TextureMap m_textures;
};

