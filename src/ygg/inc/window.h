#pragma once
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include "renderer.h"

namespace YGG
{
    struct Context;
};

class Window
{
public:
    Window(unsigned short width, unsigned short height);
    ~Window();

    bool Initialize(YGG::Context* outContext);

    bool Tick();

public:
    GLFWwindow* get() const;

private:
    unsigned short m_width;
    unsigned short m_height;


private:
    GLFWwindow* m_window;
    Renderer m_renderer;
};
