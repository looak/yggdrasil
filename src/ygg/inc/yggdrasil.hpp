#pragma once

#include <glm/vec2.hpp>
#include "platform.h"

class TextureManager;
class ShaderManager;
class Renderer;

namespace YGG
{

struct ManagersContext
{
    ManagersContext(const Renderer* renderer);
    const ShaderManager* ShaderManagerPtr;
    const TextureManager* TextureManagerPtr;
};

struct Context
{
    Renderer* RendererPtr;
};

struct DrawJob
{
    std::size_t TextureId;
    glm::vec2 Position;
    glm::vec2 Size;
};
    
}
