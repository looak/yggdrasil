#version 430

layout (location = 0) in vec2 inPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    mat4 mvp = projection * view * model;
    gl_Position = mvp * vec4(inPos, 0.0, 1.0);
}