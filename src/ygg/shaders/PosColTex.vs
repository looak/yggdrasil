#version 430

uniform mat4 MVP;

attribute vec3 vCol;
attribute vec2 vPos;
attribute vec2 vTexPos;

out vec3 ourColor;
out vec2 TexCoord;

void main()
{
    gl_Position = MVP * vec4(vPos, 0.0, 1.0);
    ourColor = vCol;
    TexCoord = vec2(vTexPos.x, vTexPos.y);
}