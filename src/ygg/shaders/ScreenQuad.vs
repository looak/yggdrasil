#version 430

layout (location = 0) in vec2 inPos;
layout (location = 1) in vec2 inCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 TexCoord;

void main()
{
    gl_Position = vec4(inPos.x, inPos.y, 0.0, 1.0);
    TexCoord = inCoord;
    
}