#pragma once

class BaseInstance
{
public:
	BaseInstance();
	virtual ~BaseInstance() = 0;

	virtual void Draw() = 0;

};
