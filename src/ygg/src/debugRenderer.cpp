#include "debugRenderer.h"
#include <logger.h>

#include <glad/glad.h>
#include <glfw/glfw3.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "utils/file_system.h"
#include "quad.h"
#include "yggdrasil.hpp"

#include <math.h>
#include "renderer.h"

void DebugRenderer::Initialize(Renderer* renderer)
{
	m_cachedRenderer = renderer;

	const ShaderManager& shaderManager = m_cachedRenderer->getShaderManager();

	m_shader = shaderManager.getShader(
		FileSystem::getPath("src/ygg/shaders/default.vs"), FileSystem::getPath("src/ygg/shaders/PinkFrg.glsl"));

	shaderManager.getShaderPtr(m_shader)->RegisterUniformInputs({ {"model", UNIFORM_INPUT::IN_MAT4},
															   {"view", UNIFORM_INPUT::IN_MAT4},
															   {"projection", UNIFORM_INPUT::IN_MAT4} });
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

	glGenBuffers(1, &m_indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

	auto descriptor = VertexLayout::Pos::getLayoutDesc();
	for (const auto& attribute : descriptor)
		VertexLayout::RegisterElement(attribute, shaderManager.getShaderPtr(m_shader)->get());

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

DebugRenderer::~DebugRenderer()
{ 
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vertexBuffer);
	glDeleteBuffers(1, &m_indexBuffer);
}

void DebugRenderer::DrawPoint(const glm::vec2& position)
{
	DebugPrimitive prim;
	prim.type = DebugPrimitive::Type::Point;
	prim.point.pos = position;

	m_debugPrimitives.push_back(prim);
}

void DebugRenderer::DrawLine(const glm::vec2& start, const glm::vec2& end)
{
	DebugPrimitive prim;
	prim.type = DebugPrimitive::Type::Line;
	prim.line.start = start;
	prim.line.end = end;

	m_debugPrimitives.push_back(prim);
}

void DebugRenderer::DrawCicle(const glm::vec2& origin, float radius)
{
	DebugPrimitive prim;
	prim.type = DebugPrimitive::Type::Circle;
	prim.circle.origin = origin;
	prim.circle.radius = radius;

	m_debugPrimitives.push_back(prim);
}


void GenerateCirlce(glm::vec2* outVertices, unsigned int* outIndices, int numVert, glm::vec2& origin, float radius)
{
	float step = 0.0f;
	float stepSize = (float)(2.0 * 3.14f / numVert);

	for (int v = 0; v < numVert; v++)
	{
		outVertices[v] = glm::vec2(radius * sin(step), radius * cos(step));
		step += stepSize;
		outIndices[v] = v;
	}
}

// NOTE: This can be optimized quite a bit by putting all lines into a big buffer and using "GL_LINES", but whatever.
void
DebugRenderer::Draw(const ViewData& viewData)
{
	glPointSize(10.0f);
	glLineWidth(5.0f);

	const ShaderManager& shaderManager = m_cachedRenderer->getShaderManager();

	shaderManager.useShader(m_shader);
	shaderManager.setUniformData(m_shader, "projection", &viewData.proj);
	shaderManager.setUniformData(m_shader, "view", &viewData.view);

	glm::mat4 model(1.f);
	model = glm::translate(model, { 0.0f, 0.0f, 0.f });
	model = glm::scale(model, glm::vec3(1.0, 1.0, 1.0f));
	shaderManager.setUniformData(m_shader, "model", &model);

	for (auto& primitive : m_debugPrimitives)
	{
		std::vector<glm::vec2> vertices;
		std::vector<unsigned int> indices;

		switch (primitive.type)
		{
		case DebugPrimitive::Type::Point:
			vertices.push_back(primitive.point.pos);
			indices.push_back(0);
			break;
		case DebugPrimitive::Type::Line:
			vertices.push_back(primitive.line.start);
			vertices.push_back(primitive.line.end);
			indices.push_back(0);
			indices.push_back(1);
			break;
		case DebugPrimitive::Type::Circle:
			vertices.resize(64);
			indices.resize(64);
			GenerateCirlce(&vertices[0], &indices[0], 64, primitive.circle.origin, primitive.circle.radius);
			break;
		}

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), vertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

		glBindVertexArray(m_vao);
		glDrawElements(GL_LINE_LOOP, (GLsizei)vertices.size(), GL_UNSIGNED_INT, nullptr);
	}

	m_debugPrimitives.clear();
}
