#pragma once
#include "platform.h"
#include <glm\glm.hpp>

class FrameBuffer
{
public:
    FrameBuffer() = default;
    ~FrameBuffer() = default;

    bool Initialize(const glm::vec2& targetDimensions);

    void use();

    u32 get();

private:
    u32 m_frameBuffer;
    u32 m_texture;
};