#include <glfw/glfw3.h>
#include "input.h"

Input* Input::m_instance;

Input::Input(GLFWwindow* window)
	:m_window(window)
{

}

Input::~Input()
{

}

void
Input::KeyCallback(GLFWwindow *window, int key, int scanCode, int action, int modifier)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void
Input::Create(GLFWwindow* window)
{
    m_instance = new Input(window);
    glfwSetKeyCallback(window, KeyCallback);
}

Input*
Input::Instance()
{
    return m_instance;
}


bool Input::GetKeyDown(int keyCode)
{
	return glfwGetKey(m_window, keyCode) == GLFW_PRESS;
}
