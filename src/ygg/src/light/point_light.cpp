#include "light/point_light.h"
#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <sstream>
#include "logger.h"

PointLight::PointLight() :
    m_shader(0),
    m_vertedBuffer(0)
{

}

PointLight::~PointLight()
{
    glDeleteVertexArrays(1, &m_vertexArrayObject);
    glDeleteBuffers(1, &m_vertedBuffer);
    glDeleteBuffers(1, &m_elementBuffer);
}

bool
PointLight::Initialize(ShaderProgram* shader)
{
    shader->RegisterUniformInputs({ {"model", UNIFORM_INPUT::IN_MAT4},
                                    {"view", UNIFORM_INPUT::IN_MAT4},
                                    {"projection", UNIFORM_INPUT::IN_MAT4} });

    m_shader = shader->getHash();
    m_shaderPtr = shader;

    const VertexLayout::Pos vertices[4] = {
                { { 0.5f, 0.5f } },
                { { 0.5f, -0.5f } },
                { { -0.5f, -0.5f } },
                { { -0.5f, 0.5f } }
    };

    const unsigned int indices[6] = {
            0, 1, 3,  // first Triangle
            1, 2, 3   // second Triangle
    };

    glGenVertexArrays(1, &m_vertexArrayObject);

    glGenBuffers(1, &m_vertedBuffer);
    glGenBuffers(1, &m_elementBuffer);

    glBindVertexArray(m_vertexArrayObject);

    glBindBuffer(GL_ARRAY_BUFFER, m_vertedBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    auto descriptor = VertexLayout::Pos::getLayoutDesc();
    for(const auto& attribute : descriptor)
        VertexLayout::RegisterElement(attribute, shader->get());

    // unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return true;
}

void
PointLight::Draw() const
{
    glBindVertexArray(m_vertexArrayObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

std::size_t
PointLight::ShaderToUse() const
{
    return m_shader;
}
