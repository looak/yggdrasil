#include "logger.h"
#include <iostream>
#include <stdio.h>

CurrentLogger::CurrentLogger(){}

CurrentLogger::~CurrentLogger()
{
    Flush();
}

void
CurrentLogger::Warning(const char *message)
{
    m_buffer.push(message);
}

void
CurrentLogger::Flush()
{
    if(freopen("output.txt", "a+", stdout))
    {
        while (!m_buffer.empty())
        {
            std::cout << m_buffer.front().c_str() << std::endl;
            m_buffer.pop();
        }

        std::fclose(stdout);
    }
    else
    {
        LOG_ERROR("Unable to open output.txt");
    }
}


