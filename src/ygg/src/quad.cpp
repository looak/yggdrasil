#include "quad.h"
#include <glm/gtc/type_ptr.hpp>

Quad::Quad() :
    m_shader(0),
    m_vertedBuffer(0),
    m_orientation(1.0f)
{

}

Quad::~Quad()
{
    glDeleteVertexArrays(1, &m_vertexArrayObject);
    glDeleteBuffers(1, &m_vertedBuffer);
    glDeleteBuffers(1, &m_elementBuffer);
}

bool
Quad::Initialize(const ShaderProgram* shader)
{
    const VertexLayout::PosTex vertices[4] = {
                { { 0.5f, 0.5f }, { 1.f, 1.f } },
                { { 0.5f, -0.5f }, { 1.f, 0.f } },
                { { -0.5f, -0.5f }, { 0.f, 0.f } },
                { { -0.5f, 0.5f }, { 0.f, 1.f } }
    };

    const unsigned int indices[6] = {
            0, 1, 3,  // first Triangle
            1, 2, 3   // second Triangle
    };

    glGenVertexArrays(1, &m_vertexArrayObject);

    glGenBuffers(1, &m_vertedBuffer);
    glGenBuffers(1, &m_elementBuffer);

    glBindVertexArray(m_vertexArrayObject);

    glBindBuffer(GL_ARRAY_BUFFER, m_vertedBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    m_shader = shader->getHash();

    auto descriptor = VertexLayout::PosTex::getLayoutDesc();
    for(const auto& attribute : descriptor)
        VertexLayout::RegisterElement(attribute, shader->get());

    // unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return true;
}

void
Quad::Draw() const
{
	glBindVertexArray(m_vertexArrayObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

std::size_t
Quad::ShaderToUse() const
{
    return m_shader;
}

const glm::mat4
Quad::getOrientation() const
{
    return m_orientation;
}
