#pragma once
#include "resource/shader_program.h"
#include <glm/mat4x4.hpp>
#include "resource/input_layout.h"

class Quad
{
public:
    Quad();
    ~Quad();

    bool Initialize(const ShaderProgram* shader);
    void Draw() const;
    std::size_t ShaderToUse() const;
    const glm::mat4 getOrientation() const;

private:
    unsigned int m_vertexArrayObject;
    unsigned int m_vertedBuffer;
    unsigned int m_elementBuffer;
    std::size_t m_shader;

    glm::mat4 m_orientation;
};
