#include "quad_instance.h"
#include "shader_manager.h"
#include "quad.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

QuadInstance::QuadInstance(const Quad *quadPtr) :
	m_quadPtr(quadPtr),
	m_orientation(1.f),
	m_size(0),
	m_position(0)
{
}

QuadInstance::~QuadInstance()
{
    m_quadPtr = nullptr;
}

void
QuadInstance::Draw(const ShaderManager& shaderManager) const
{
	auto shader = m_quadPtr->ShaderToUse();
	glm::mat4 model(1.f);
	model = glm::translate(model, m_position);
	model = glm::scale(model, glm::vec3(m_size, 0.0f));
	shaderManager.setUniformData(shader, "model", &model);

	m_quadPtr->Draw();
}

void
QuadInstance::setPosition(const glm::vec3 &pos)
{
    m_position = pos;
}

std::size_t
QuadInstance::ShaderToUse() const
{
    return m_quadPtr->ShaderToUse();
}

void
QuadInstance::setSize(const glm::vec2 &size)
{
    m_size = size;
}
