#pragma once

#include "base_instance.h"
#include <glm/detail/type_mat4x4.hpp>

class Quad;
class ShaderManager;

class QuadInstance /*:
		public BaseInstance*/
{
public:
	QuadInstance(const Quad* quadPtr);
	~QuadInstance();
	void Draw(const ShaderManager& shaderManager) const;

    void setPosition(const glm::vec3& pos);
    void setSize(const glm::vec2& size);

    std::size_t ShaderToUse() const;

private:
	const Quad* m_quadPtr;
	glm::mat4 m_orientation;
    glm::vec3 m_position;
	glm::vec2 m_size;
};
