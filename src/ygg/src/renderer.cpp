#include "renderer.h"
#include <logger.h>

#include <glad/glad.h>
#include <glfw/glfw3.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "utils/file_system.h"
#include "quad.h"
#include "light/point_light.h"
#include "yggdrasil.hpp"
#include "debugRenderer.h"
#include "framebuffer.h"

Renderer::~Renderer()
{ }

void
Renderer::Draw(GLFWwindow* window)
{
    float ratio;
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    ratio = width / (float) height;
    glViewport(0, 0, width, height);

    int halfWidth, halfHeight;
    halfWidth = (int)(width * .5f);
    halfHeight = (int)(height * .5f);
    
    glm::mat4 p(1.0f);
    glm::mat4 view(1.0f);

/*    m_firstFrameBuffer->use();

    glClearColor(0.10f, 0.0f, 0.10f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    */
    p = glm::ortho(-(float)halfWidth, (float)halfWidth, -(float)halfHeight, (float)halfHeight, -1.f, 1.f);

    /*
    // glEnable(GL_DEPTH_TEST);
    for(auto drawJob : m_drawJobs)
    {
        m_textureManager.useTexture(drawJob.TextureId);
        const auto shader = m_instance->ShaderToUse();
        m_shaderManager.useShader(shader);
        m_shaderManager.setUniformData(shader, "projection", &p);
        m_shaderManager.setUniformData(shader, "view", &view);

        glm::mat4 model(1.f);
        model = glm::translate(model, {drawJob.Position, 0.f});
        model = glm::scale(model, glm::vec3(drawJob.Size, 0.0f));
        m_shaderManager.setUniformData(shader, "model", &model);

        m_instance->Draw();
    }
    
    m_secondFrameBuffer->use();*/
    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    {
        const auto shader = m_pointLight->ShaderToUse();
        m_shaderManager.useShader(shader);        
        m_shaderManager.setUniformData(shader, "projection", &p);
        m_shaderManager.setUniformData(shader, "view", &view);
        
        glm::mat4 model(1.f);
        model = glm::translate(model, { 256.f, 256.f, 0.f });
        model = glm::scale(model, glm::vec3(128.f, 128.f, 0.0f));
        m_shaderManager.setUniformData(shader, "model", &model);

        m_pointLight->Draw();
    }
    
    // unset framebuffer
    /*glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);

    // clear buffer
    glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);*/

    //glBindTexture(GL_TEXTURE_2D, m_secondFrameBuffer->get());
    //glBindTexture(GL_TEXTURE_2D, m_firstFrameBuffer->get());
    //m_shaderManager.useShader(m_screenQuad->ShaderToUse());
    /*glActiveTexture(1);
    glBindTexture(GL_TEXTURE_2D, m_secondFrameBuffer->get());*/
    
    //m_screenQuad->Draw(); 
    
    /*ViewData viewData = {};
    viewData.view = view;
    viewData.proj = p;
    DebugRenderer::GetInstance().Draw(viewData);*/
    
    glfwSwapBuffers(window);
    glfwPollEvents();
    
    m_drawJobs.clear();
}

void
Renderer::Initialize(GLFWwindow* window)
{
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glm::vec2 windowDimensions(width, height);

    m_firstFrameBuffer = new FrameBuffer();
    m_firstFrameBuffer->Initialize(windowDimensions);
    m_secondFrameBuffer = new FrameBuffer();
    m_secondFrameBuffer->Initialize(windowDimensions);

    auto quad = new Quad();
    auto screenQuad = new Quad();
    auto pointLight = new PointLight();

    auto screenShader = m_shaderManager.getShader(
            FileSystem::getPath("src/ygg/shaders/ScreenQuad.vs"), FileSystem::getPath("src/ygg/shaders/ScreenQuad.fs"));

    auto shader = m_shaderManager.getShader(
            FileSystem::getPath("src/ygg/shaders/PosTex.vs"), FileSystem::getPath("src/ygg/shaders/PosTex.fs"));
    
    auto pointLightShader = m_shaderManager.getShader(
        FileSystem::getPath("src/ygg/shaders/PointLight.vs"), FileSystem::getPath("src/ygg/shaders/PointLight.fs"));

    m_shaderManager.getShaderPtr(shader)->RegisterUniformInputs({{"model", UNIFORM_INPUT::IN_MAT4},
                                                               {"view", UNIFORM_INPUT::IN_MAT4},
                                                               {"projection", UNIFORM_INPUT::IN_MAT4}});



    auto plShaderPtr = m_shaderManager.getShaderPtr(pointLightShader);
    auto screenShaderPtr = m_shaderManager.getShaderPtr(screenShader);

    /*screenShaderPtr->RegisterUniformInputs({{"Geo", UNIFORM_INPUT::IN_INT}, {"Light", UNIFORM_INPUT::IN_INT}});
    int data = 0;
    screenShaderPtr->setUniformData("Geo", &data);
    data = 1;
    screenShaderPtr->setUniformData("Light", &data);*/

    if (!quad->Initialize(m_shaderManager.getShaderPtr(shader)))
        LOG_ERROR("Failed to initialize Quad");
    
    if (!pointLight->Initialize(plShaderPtr))
        LOG_ERROR("Failed to initialize PointLight");

    if (!screenQuad->Initialize(screenShaderPtr))
        LOG_ERROR("Failed to initialize ScreenQuad");

    m_instance = quad;
    m_pointLight = pointLight;
    m_screenQuad = screenQuad;

    //DebugRenderer::GetInstance().Initialize(this);
}

const ShaderManager&
Renderer::getShaderManager() const
{
    return m_shaderManager;
}

const TextureManager&
Renderer::getTextureManager() const
{
    return m_textureManager;
}

void
Renderer::SetDrawContext(std::vector<YGG::DrawJob> drawJobs)
{
    m_drawJobs.insert(m_drawJobs.end(), drawJobs.begin(), drawJobs.end());
}