#include <fstream>
#include <sstream>
#include <glad/glad.h>
#include "fragment_shader.h"
#include "logger.h"

FragmentShader::FragmentShader(const std::string &filepath) :
    m_filepath(filepath)
{

}

FragmentShader::~FragmentShader()
{
    glDeleteShader(m_shader);
}

bool
FragmentShader::Initialize()
{
    std::ifstream filestream;
    std::string shaderStr;
    filestream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        filestream.open(m_filepath.c_str());
        std::stringstream buffer;
        buffer << filestream.rdbuf();
        filestream.close();
        shaderStr = buffer.str();
    }
    catch(std::ifstream::failure e)
    {
        LOG_ERROR(e.what());
        return false;
    }

    return Compile(shaderStr, GL_FRAGMENT_SHADER);
}

unsigned int
FragmentShader::get() const
{
    return m_shader;
}
