#pragma once
#include <string>
#include "shader_base.h"

class FragmentShader : public ShaderBase
{
public:
    explicit FragmentShader(const std::string& filepath);
    ~FragmentShader();

    unsigned int get() const;
    bool Initialize();

private:
    std::string m_filepath;
};