#pragma once

#include <logger.h>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <vector>
#include <string>

namespace VertexLayout
{
struct AttributeDiscriptor
{
    AttributeDiscriptor(std::string atr, std::size_t size, GLenum type, bool norm, std::size_t stride, std::size_t offset) :
        AttributeName(atr), ElementSize(size), ElementType(type), Normalized(norm), Stride(stride), Offset(offset)
    {

    }
    std::string AttributeName;
    std::size_t ElementSize;
    GLenum ElementType;
    bool Normalized;
    std::size_t Stride;
    std::size_t Offset;
};


typedef std::vector<AttributeDiscriptor> LayoutDescriptor;
// this is tightly coupled with the inputs of PosColTex.vs
struct TwoDPosColTex
{
    static LayoutDescriptor getLayoutDesc()
    {
        LayoutDescriptor result;
        result.emplace_back("vPos", 2, GL_FLOAT, false, sizeof(TwoDPosColTex), 0);
        result.emplace_back("vCol", 3, GL_FLOAT, false, sizeof(TwoDPosColTex), sizeof(float)*2);
        result.emplace_back("vTexPos", 2, GL_FLOAT, false, sizeof(TwoDPosColTex), sizeof(float)*5);
        return result;
    }

    glm::vec2 position;
    glm::vec3 color;
    glm::vec2 texcoords;
};

struct PosTex
{
	static LayoutDescriptor getLayoutDesc()
	{
		LayoutDescriptor result;
		result.emplace_back("inPos", 2, GL_FLOAT, false, sizeof(PosTex), 0);
		result.emplace_back("inCoord", 2, GL_FLOAT, false, sizeof(PosTex), sizeof(float)*2);
		return result;
	}

	glm::vec2 position;
	glm::vec2 texcoord;
};

struct Pos
{
	static LayoutDescriptor getLayoutDesc()
	{
		LayoutDescriptor result;
		result.emplace_back("inPos", 2, GL_FLOAT, false, sizeof(PosTex), 0);
		return result;
	}

	glm::vec2 position;
};

struct PointLight
{
    static LayoutDescriptor getLayoutDesc()
    {
		LayoutDescriptor result;
        result.emplace_back("inPos", 2, GL_FLOAT, false, sizeof(PointLight), 0);
        result.emplace_back("inTexCoord", 2, GL_FLOAT, false, sizeof(PointLight), sizeof(glm::vec2));
        result.emplace_back("inColor", 3, GL_FLOAT, false, sizeof(PointLight), sizeof(glm::vec2) + sizeof(glm::vec2));
        return result;
    }

    glm::vec2 position;
    glm::vec2 texcoord;
    glm::vec3 color;
};

static void RegisterElement(const AttributeDiscriptor& descriptor, const int shader)
{
    auto location = glGetAttribLocation(shader, descriptor.AttributeName.c_str());

    if (location < 0)
        LOG_ERROR("Attribute location not found. Make sure you're using correct layout!");

    glEnableVertexAttribArray(location);
    glVertexAttribPointer(location, (GLint)descriptor.ElementSize, descriptor.ElementType,
            descriptor.Normalized, (GLsizei)descriptor.Stride, (void*)descriptor.Offset);
}

}