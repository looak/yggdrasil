#include "shader_base.h"
#include <glad/glad.h>
#include "logger.h"

bool
ShaderBase::Compile(const std::string& source, u32 shaderType)
{
    int success;
    const char* sourceAry = source.c_str();
    m_shader = glCreateShader(shaderType);
    glShaderSource(m_shader, 1, &sourceAry, nullptr);
    glCompileShader(m_shader);

    glGetShaderiv(m_shader, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        const unsigned short logSize = 512;
        char infoLog[logSize];
        glGetShaderInfoLog(m_shader, logSize, nullptr, infoLog);
        LOG_ERROR(infoLog);
        return false;
    }
    return true;
}