#include <string>
#include <platform.h>

class ShaderBase
{
public:
    ShaderBase() = default;
    virtual ~ShaderBase() = default;

    virtual unsigned int get() const = 0;
    virtual bool Initialize() = 0;

    bool Compile(const std::string& source, u32 shaderType);

protected:
    unsigned int m_shader;
};  