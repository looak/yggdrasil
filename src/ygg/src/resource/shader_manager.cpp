#include "shader_manager.h"

#include <logger.h>
#include "shader_program.h"

ShaderManager::~ShaderManager()
{
    for(auto pair : m_shaders)
        delete pair.second;
}

std::size_t
ShaderManager::getShader(const std::string &vsLocation, const std::string &fsLocation) const
{
    ShaderProgram* shdr = new ShaderProgram(vsLocation, fsLocation);
    auto curHash = shdr->getHash();

    if(m_shaders.find(curHash) == m_shaders.end())
    {
        m_shaders[curHash] = shdr;
        m_shaders[curHash]->Initialize();
    }
    else
        delete shdr;

    return curHash;
}

unsigned int
ShaderManager::getShader(std::size_t hash) const
{
    if(m_shaders.find(hash) == m_shaders.end())
    {
        LOG_WARNING("Shader doesn't exist!");
        return 0;
    }

    return m_shaders.at(hash)->get();
}

void
ShaderManager::useShader(std::size_t hash) const
{
    if(m_shaders.find(hash) == m_shaders.end())
    {
        LOG_ERROR("Shader doesn't exist!");
    }

    m_shaders.at(hash)->use();
}

const ShaderProgram*
ShaderManager::getShaderPtrConst(std::size_t hash) const
{
    if(m_shaders.find(hash) == m_shaders.end())
    {
        LOG_WARNING("Shader doesn't exist!");
        return nullptr;
    }

    return m_shaders.at(hash);
}

ShaderProgram*
ShaderManager::getShaderPtr(std::size_t hash) const
{
    if(m_shaders.find(hash) == m_shaders.end())
    {
        LOG_WARNING("Shader doesn't exist!");
        return nullptr;
    }

    return m_shaders.at(hash);
}

bool
ShaderManager::setUniformData(std::size_t hash, const std::string &name, const void *data) const
{
    if(m_shaders.find(hash) == m_shaders.end())
    {
        LOG_WARNING("Shader doesn't exist!");
        return false;
    }
    m_shaders.at(hash)->setUniformData(name, data);
    return true;
}
