#include <logger.h>
#include <sstream>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>
#include "shader_program.h"

ShaderProgram::ShaderProgram(const std::string &vsLocation, const std::string &fsLocation) :
    m_vs(vsLocation),
    m_fs(fsLocation)
{
    std::ostringstream strStream;
    strStream << vsLocation;
    strStream << fsLocation;
    m_hash = std::hash<std::string>{}(strStream.str());
}


ShaderProgram::~ShaderProgram()
{
}

bool
ShaderProgram::Initialize()
{
    if (!m_vs.Initialize())
        return false;

    if(!m_fs.Initialize())
        return false;

    m_program = glCreateProgram();

    glAttachShader(m_program, m_vs.get());
    glAttachShader(m_program, m_fs.get()); 
    glLinkProgram(m_program);

    int success = 0;
    glGetProgramiv(m_program, GL_LINK_STATUS, &success);
    if (!success)
    {
        const unsigned short logSize = 512;
        char infoLog[logSize];
        glGetProgramInfoLog(m_program, logSize, nullptr, infoLog);
        LOG_ERROR(infoLog);
    }

    return true;
}

unsigned int
ShaderProgram::get() const
{
    return m_program;
}

void
ShaderProgram::use()
{
    glUseProgram(m_program);
}

std::size_t
ShaderProgram::getHash() const
{
    return m_hash;
}

void
ShaderProgram::RegisterUniformInputs(const std::vector<std::pair<std::string, UNIFORM_INPUT>> inputs)
{
    for(const auto& pair : inputs)
    {
        auto result = glGetUniformLocation(m_program, pair.first.c_str());
        std::pair<int, UNIFORM_INPUT> insert(result, pair.second);
        m_inputs[pair.first] = insert;
    }
}

bool
ShaderProgram::setUniformData(const std::string &name, const void *data) const
{
    if (m_inputs.find(name) == m_inputs.end())
    {
        LOG_WARNING("Shader doesnt' have given input registered.");
        return false;
    }

    auto pair = m_inputs.at(name);
    return internalSet(pair.first, pair.second, data);
}

bool
ShaderProgram::internalSet(int target, UNIFORM_INPUT type, const void* data) const
{
    switch (type)
    {
        case IN_INT:
        {
            glUniform1i(target, *(int*)data);
            return true;
        }
        case IN_FLOAT:
        {
            glUniform1f(target, *(float*)data);
            return true;
        }
        case IN_FLOAT2:
        {
            auto tmpVec = (glm::vec2 *) data;
            glUniform2f(target, tmpVec->x, tmpVec->y);
            return true;
        }
        case IN_FLOAT4:
        {
            auto tmpVec = (glm::vec4 *) data;
            glUniform4f(target, tmpVec->x, tmpVec->y, tmpVec->z, tmpVec->w);
            return true;
        }
        case IN_MAT4:
        {
            glUniformMatrix4fv(target, 1, GL_FALSE, (float *) data);
            return true;
        }        
        default:
        {
            LOG_WARNING("Shader input type not implemented or doesn't exist");
            return false;
        }
    }
}

