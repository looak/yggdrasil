#pragma once

#include "vertex_shader.hpp"
#include "fragment_shader.h"

#include <vector>
#include <map>

enum UNIFORM_INPUT
{
    IN_FLOAT,
    IN_INT,
    IN_FLOAT2,
    IN_FLOAT4,
    IN_MAT4
};

class ShaderProgram
{
public:
    ShaderProgram(const std::string& vsLocation, const std::string& fsLocation);
    ~ShaderProgram();

    bool Initialize();
    void RegisterUniformInputs(const std::vector<std::pair<std::string, UNIFORM_INPUT>> inputs);

    std::size_t getHash() const;
    unsigned int get() const;
    void use();

    bool setUniformData(const std::string &name, const void *data) const;

private:
    bool internalSet(int target, UNIFORM_INPUT type, const void* data) const;
    std::map<std::string, std::pair<int, UNIFORM_INPUT>> m_inputs;

    std::size_t m_hash;
    unsigned int m_program;
    VertexShader m_vs;
    FragmentShader m_fs;

};

