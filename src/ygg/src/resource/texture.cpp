#include "texture.h"
#include <glad/glad.h>
#include <stb/stb_image.h>
#include <logger.h>
#include <sstream>

Texture::Texture(const std::string &filepath) :
    m_filepath(filepath), m_texture(0)
{
    m_hash = std::hash<std::string>{}(filepath);
}

Texture::~Texture()
{

}

unsigned int
Texture::get() const
{
    return m_texture;
}

bool
Texture::Initialize()
{
    glGenTextures(1, &m_texture);
    glBindTexture(GL_TEXTURE_2D, m_texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // setUniformData the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// setUniformData texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // setUniformData texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    return LoadTexture();
}

bool
Texture::LoadTexture()
{
    int width, hegith, nrChannels = 0;
    unsigned char* textureData = stbi_load(m_filepath.c_str(), &width, &hegith, &nrChannels, 0);

    if(nrChannels == 4)
        nrChannels = GL_RGBA;
    else
        nrChannels = GL_RGB;   

    if(textureData)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, nrChannels, width, hegith, 0, nrChannels, GL_UNSIGNED_BYTE, textureData);
        glGenerateMipmap(GL_TEXTURE_2D);

        // unbind
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    else
    {
        std::ostringstream ss;
        ss << "Unable to load texture: " <<  m_filepath << " : " << stbi_failure_reason();
        LOG_WARNING(ss.str().c_str());
        return false;
    }

    stbi_image_free(textureData);
    return true;
}

std::size_t
Texture::getHash()
{
    return m_hash;
}



