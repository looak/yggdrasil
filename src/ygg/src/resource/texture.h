#pragma once
#include <string>

class Texture
{
public:
    explicit Texture(const std::string& filepath);
    ~Texture();

    unsigned int get() const;
    bool Initialize();

    std::size_t getHash();

private:
    bool LoadTexture();

private:
    std::size_t m_hash;
    std::string m_filepath;
    unsigned int m_texture;
};