#include <glad/glad.h>
#include <logger.h>
#include "texture_manager.h"
#include "texture.h"

TextureManager::~TextureManager()
{
    for(auto pair : m_textures)
        delete pair.second;
}

std::size_t
TextureManager::getTexture(const std::string &texLocation) const
{
    Texture* tex = new Texture(texLocation);
    auto hash = tex->getHash();
    if(m_textures.find(hash) == m_textures.end())
    {
        m_textures[hash] = tex;
        m_textures[hash]->Initialize();
    }
    else
        delete tex;

    return hash;
}

unsigned int
TextureManager::getTexture(std::size_t hash) const
{
    if(m_textures.find(hash) == m_textures.end())
    {
        LOG_WARNING("Texture hash did not exist!");
        return 0;
    }

    return m_textures.at(hash)->get();
}

void
TextureManager::useTexture(std::size_t hash) const
{
    if(m_textures.find(hash) == m_textures.end())
    {
        LOG_WARNING("Texture hash did not exist!");
        return;
    }

    glBindTexture(GL_TEXTURE_2D, m_textures.at(hash)->get());
}
