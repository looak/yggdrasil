#include <fstream>
#include <sstream>
#include "vertex_shader.hpp"
#include "logger.h"

VertexShader::VertexShader(const std::string& filepath) :
    m_filepath(filepath)
{

}

VertexShader::~VertexShader()
{
    glDeleteShader(m_shader);
}

bool
VertexShader::Initialize()
{
    std::ifstream filestream;
    std::string vertexShader;
    filestream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        filestream.open(m_filepath.c_str());
        std::stringstream buffer;
        buffer << filestream.rdbuf();
        filestream.close();
        vertexShader = buffer.str();
    }
    catch(std::ifstream::failure e)
    {
        LOG_ERROR(e.what());
        return false;
    }

    return Compile(vertexShader);
}

bool
VertexShader::Compile(const std::string& source)
{
    int success;
    const char* sourceAry = source.c_str();
    m_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(m_shader, 1, &sourceAry, nullptr);
    glCompileShader(m_shader);

    glGetShaderiv(m_shader, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        const unsigned short logSize = 512;
        char infoLog[logSize];
        glGetShaderInfoLog(m_shader, logSize, nullptr, infoLog);
        LOG_ERROR(infoLog);
        return false;
    }
    return true;
}

unsigned int
VertexShader::get() const
{
    return m_shader;
}
