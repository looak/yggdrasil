#pragma once
#include <string>
#include <glad/glad.h>

class VertexShader
{
public:
    explicit VertexShader(const std::string& filepath);
    ~VertexShader();

    unsigned int get() const;
    bool Initialize();

private:
    bool Compile(const std::string& source);

private:
    std::string m_filepath;
    unsigned int m_shader;
};