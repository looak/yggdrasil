#pragma once

#include <map>
#include <string>
//#include <filesystem>
#include <chrono>

struct FileMeta
{
    std::size_t m_hash;
    std::string m_filePath;
    std::chrono::time_point<std::chrono::system_clock> m_lastChanged;
  //  std::filesystem::file_time_type m_lastChanged;
};

class FileListner
{  
public:
    virtual ~FileListner() = 0;
    virtual bool HandleEvent(const FileMeta& file) = 0;

};

class FileWatcher
{
public:
    FileWatcher() = default;

    void Update();


private:
    typedef std::chrono::duration<int, std::milli> Delay;
    Delay m_delay;

    typedef std::chrono::time_point<std::chrono::system_clock> TimePoint; 
    TimePoint m_lastUpdate;

    typedef std::map<std::size_t, FileMeta> FilesMap;
    FilesMap m_watching;
    
    typedef std::map<std::size_t, FileListner*> ListnerMap;
    ListnerMap m_listners;
};