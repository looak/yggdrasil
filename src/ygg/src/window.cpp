#include "window.h"
#include "logger.h"
#include "yggdrasil.hpp"

void GLAPIENTRY
MessageCallback(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar* message,
    const void* userParam)
{
   //LOG_ERROR()
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
        (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
        type, severity, message);
}


Window::Window(unsigned short width, unsigned short height) :
 m_width(width),
 m_height(height),
 m_window(nullptr)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
}

Window::~Window()
{
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

bool Window::Initialize(YGG::Context* outContext)
{
    if (!glfwInit())
    {
        LOG_ERROR("Failed to initialize GLFW");
        return false;
    }


    m_window = glfwCreateWindow(m_width, m_height, "Yggdrasil", nullptr, nullptr);

    if (!m_window)
    {
        glfwTerminate();
        LOG_ERROR("Failed to initialize Window");
        return false;
    }

    glfwMakeContextCurrent(m_window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);

    m_renderer.Initialize(m_window);

    outContext->RendererPtr = &m_renderer;
    return true;
}

bool
Window::Tick()
{
    m_renderer.Draw(m_window);

    return !glfwWindowShouldClose(m_window);
}

GLFWwindow*
Window::get() const
{
    return m_window;
}
