#include "yggdrasil.hpp"
#include "renderer.h"

namespace YGG
{
    ManagersContext::ManagersContext(const Renderer* renderer)
    {
        ShaderManagerPtr = &renderer->getShaderManager();
        TextureManagerPtr = &renderer->getTextureManager();
    }
};
